### Pinephone seek music with Volume Buttons
This a small python-script that enables a feature I really miss on the pinephone. Being able to seek music-tracks with the volume buttons on the phone or wired headphones with a remote on them. This little python script enables that feature on the pinephone.

### needed dependencies
This script uses `python 3.5+`, `python-evdev` for the script, and `playerctl` to send the correct commands to the running (music)-player. It should be fairly distro agnostic really. But please feel free to test it on your pinephones and report back to me.

For an arch-based distribution you can install the dependencies with this command (python should be there already, but I added it for good measure):
```
sudo pacman -S python python-evdev playerctl
```

On other distros you need to install the needed packages accordingly. Which ones you need will depend on the distro you run on your pinephone. Feel free to open a Pull Request to add you distro to the list.

### How do I use the script?
Using the script is rather simple, but not really that convenient and (maybe) not ready for productive use at the moment.

First download this repo to your pinephone. To run the script just type this into the terminal of your pinephone (this assumes you are in the directory, where the script is located):

```
python music-seeker.py
```

If this does not work you might need to use this command instead depending on the distro:

```
python3 music-seeker.py
```

### What does work and what does not work?
* Right now this script does only work when using either the hardware-buttons (Volume Up and Volume down) or a wired headset, which is plugged in into the headphone jack on the pinephone since these devices are the only ones being specifically monitored. The devices should be the same on each pinephone really.

* Bluetooth Headphones with controls don't work with this script for now. The bluetooth-devices get added as an extra device once connected, so I can not hardcode a value for those into this script. Depending on how many input-devices are connected to the pinephone (keyboard, mouse, etc.) the bluetooth-headphone might another device-number.

* Action is now triggered via holding Volume Up or Volume Down for more than 300ms AND releasing the button afterwards.

* Headphone-controls are a bit wonky on the pinephone itself right now, see [here](https://gitlab.com/postmarketOS/pmaports/-/issues/975)

### What's next for this project?
* Make the script an interrupt rather than a continously listening service to preserve recources on the pinephone. I don't really know how I could accomplish that really. 

* adjust music volume after action is triggered back to original level before music was seeked. 

* Look into if long button press is also possible to measure in a different way. Right now you have to press the button for 300ms and release it. Certainly works but maybe I can do it in another way where I don't have to release the button?

* maybe add an option instead holding the button for 300ms you double tap it to seek music. 

* Get this working on bluetooth devices as well. This seems rather hard, since newly connected input-devices don't get a fixed event identifier. What the bluetooth device gets depends on how many other devices are connected.

* add a systemd-service-file to the project to make starting and stopping this script more easy.

* packages this up for various distributions and get it in the repos so it is easily accessible to anyone.  

### Would like help?
I am not really well versed in python and this script is rather hacked together using some tutorials and code snippets and some trial and error. If you would like to add anything to this project feel free to open an issue or submit a pull request. Every help is appereciated. 
