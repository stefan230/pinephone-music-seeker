import os
import asyncio
import time
from evdev import InputDevice, categorize, ecodes
#event 4 should be the device for the volume-buttons on each pinephone. Though that needs to be checked!
button = InputDevice('/dev/input/event4')
#event 5 is the headphone jack of the pinephone
headphone = InputDevice('/dev/input/event5')

async def print_events(device):
    async for event in device.async_read_loop():
     if event.type == ecodes.EV_KEY:
     # event code 115 = VOLUMEUP
      if event.code == 115:
      # pinephone has binary buttons. It is either one for down and 0 for up. Therefore we check if the button is pressed or not
       if event.value == 1:
        tic = time.perf_counter()
       if event.value == 0:
        toc = time.perf_counter()
        zeit = toc - tic
        #print (zeit)
        if zeit  < 0.3:
         pass
        elif zeit > 0.3:
         os.system("playerctl next")       
     # event code 114 = VOLUMEDOWN
      elif event.code == 114:
       if event.value == 1:
        tic = time.perf_counter()
       if event.value == 0:
        toc = time.perf_counter()
        zeit = toc - tic
        #print (zeit)
        if zeit  < 0.3:
         pass
        elif zeit > 0.3:
         os.system("playerctl previous")

for device in button, headphone:
    asyncio.ensure_future(print_events(device))

loop = asyncio.get_event_loop()
loop.run_forever()
